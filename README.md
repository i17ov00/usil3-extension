# Extension des Jenkins Shared Librairies 
Projet apportant des nouvelles méthodes custom pour les Shared Libraries de Jenkins.

# Fonctions 

## myProjectCheckout

Checkout du projet Git.

### Description

Permet de récupérer les sources du projet.

Une fois le projet récupéré, les informations sur le projet sont extraites depuis l'url Git et les variables suivantes sont initialisées :

* **env.gitProjectName** : nom du repository git
* **env.trigrammeAppli** : trigramme
* **env.codeAppli** : code applicatif
* **env.projectName** : le nom du projet

L'information de dernière version tagguée est également mise à disposition :

* **env.gitTag** : tag le plus récent du projet

/!\ Cette version est nécessaire pour faire fonctionner le mode release, grace à l'ajout de l'extension :

```groovy
[$class: 'LocalBranch', localBranch: env.BRANCH_NAME]
```

### Paramètres
Aucun.

## myBuildMaven

Permet de faire le build d'un projet maven soit en mode SNAPSHOT soit en mode RELEASE.

### Description

L'exécution se fait en plusieurs étapes : 

* build de l'artefact
* lancement des tests unitaires
* création du rapport Sonar
* release maven (optionnelle)
* déploiement sur nexus

Dans le cas d'une release : 

* lancement de la commande maven release:prepare et release:perform 

En fin de traitement, la version de l'artefact buildé (version du pom.xml) est stockée dans la variable :

* **env.artefactVersion**

### Paramètres (optionnels)
* **modeRelease** (valeur par défaut : false) : si modeRelease = true, une release maven sera réalisée à la suite du build si tout s'est bien passé. Peut être laissé à false pour un SNAPSHOT.
* **gitCredentialsId** (valeur par défaut : '') : Credentials Bitbucket permettant de commit et de tag lors d'une release maven. Peut être laissé à vide pour un build SNAPSHOT.
* **dockerImageMaven** (valeur par défaut : usilParams.dockerRegistryImageMavenBuild) : image docker à utiliser pour build. 
* **mvnOptionnalArgs** (valeur par défaut : '') : arguments additionnels pour le build maven

## myBuildAngular

Permet de faire le build d'un projet Front Angular soit en mode Beta soit en mode RELEASE.

La fonction se base sur les scripts déclarés dans le package.json.

Elle s'attend à pouvoir lancer les scripts suivants :
* npm run build:prod : pour lancer le build en mode prod 
* npm run test : pour lancement des TU
* npm run lint : pour lancer le linter
* npm run e2e : pour lancer les tests e2e
* npm 
Si un des scripts n'est pas présent, l'étape est simplement ignorée.

### Description

L'exécution se fait en plusieurs étapes : 

* build du projet en mode prod
* lancement du lint, des tests unitaires et des tests E2E
* en cas de release :
    * set de la version dans le package.json sans le -beta
    * tag git
    * publication sur le nexus
    * increment de la version du package.json en *-beta  

En fin de traitement, la version de l'artefact buildé (version dans le package.json) est stockée dans la variable :

* **env.artefactVersion**

### Paramètres (optionnels)
* **modeRelease** (valeur par défaut : false) : si modeRelease = true, une release maven sera réalisée à la suite du build si tout s'est bien passé. Peut être laissé à false pour un SNAPSHOT.
* **gitCredentialsId** (valeur par défaut : '') : Credentials Bitbucket permettant de commit et de tag lors d'une release. Peut être laissé à vide pour un build SNAPSHOT.
* **dockerImageName** (valeur par défaut : usilParams.dockerRegistryImageNodeBuild) : image docker à utiliser pour build. 
* **runTests** (valeur par défaut : true) : pour activer ou désactiver le lancement des tests unitaires
* **runTestsE2E** (valeur par défaut : true) : pour activer ou désactiver le lancement des E2E 

