def getPackageJsonVersion() {
	def package_json_path = "${WORKSPACE}/package.json";
	def package_json_model = readJSON file: package_json_path;
	return package_json_model.version;
}

def isScriptPresentInPackageJson(def scriptName) {
	def package_json_path = "${WORKSPACE}/package.json";
	def package_json_model = readJSON file: package_json_path;
	return package_json_model.scripts[scriptName] != null;
}

def call(def isRelease = false, def gitCredentialsId = '', dockerImageName = usilParams.dockerRegistryImageNodeBuild, dockerRegistry = usilParams.dockerRegistryUrl, runTests = true, runTestsE2E = false) {
	def last_git_comment = sh(returnStdout: true, script: 'git log -1 --pretty=%B')
	if (!last_git_comment.contains("[npm-version]")) {
		docker.withRegistry(dockerRegistry) {
			docker.image(dockerImageName).inside("--entrypoint=''") {
				stage('Build') {
					sh "npm config set cafile /home/jenkins/root.cer"
					sh "npm ci --registry ${usilParams.nexusRepoNpm}"
					if (isScriptPresentInPackageJson("build:prod")) {
						sh 'npm run build:prod'
					} else if (isScriptPresentInPackageJson("build")) {
						sh 'npm run build'
					} else {
						echo "Warning : Script 'build:prod' ou 'build' non trouvé dans le package.json"
					}
				}
				if (runTests || runTestsE2E) {
					stage('Qualimetry') {
						parallel(
							"Tests": {
								if (runTests) {
									if (isScriptPresentInPackageJson("test")) {
										sh 'npm run test'
									} else {
										echo "Warning : Script 'test' non trouvé dans le package.json"
									}
								} else {
									echo "Info : Skip tests E2E..."
								}
							},
							"Lint": {
								if (isScriptPresentInPackageJson("lint")) {
									sh 'npm run lint'
								} else {
									echo "Warning : Script 'lint' non trouvé dans le package.json"
								}
							},
							"e2e": {
								if (runTestsE2E) {
									if (isScriptPresentInPackageJson("e2e")) {
										sh 'npm run e2e'
									} else {
										echo "Warning : Script 'e2e' non trouvé dans le package.json"
									}
								} else {
									echo "Info : Skip tests E2E..."
								}
							}
						)
					}
				} else {
					echo "Info : Skip qualimetry"
				}

				stage('NPM Pack') {
					sh('npm pack')
				}

				if (isRelease) {
					withCredentials([usernamePassword(credentialsId: gitCredentialsId, usernameVariable: 'gitPushUsername', passwordVariable: 'gitPushPassword')]) {
						echo "Npm version release ..."
						sh "git config user.email '${env.trigrammeAppli}@b1envenue.onmicrosoft.com'"
						sh "git config user.name '$env.trigrammeAppli'"
						def remoteUrl = sh(returnStdout: true, script: "git remote get-url origin").trim();
						echo remoteUrl;
						// ajout des credentials dans l'url
						def indexProtocol = remoteUrl.indexOf("//");
						def encoded_username = URLEncoder.encode("${gitPushUsername}",'UTF-8');
						def encoded_password = URLEncoder.encode("${gitPushPassword}",'UTF-8');
						remoteUrl = remoteUrl.substring(0, indexProtocol + 2) + "${encoded_username}:${encoded_password}@" + remoteUrl.substring(indexProtocol + 2)
						echo remoteUrl;
						sh "git remote set-url --push origin ${remoteUrl}";
						stage('NPM version release') {
							sh 'npm version patch --no-git-tag-version'
							env.artefactVersion = getPackageJsonVersion();

							sh 'git add package.json'
							sh "git commit -m '[npm version] version RELEASE ${env.artefactVersion}'"
							sh "git tag ${env.artefactVersion}"
							sh "git push -u origin ${env.gitBranchName}"
							sh "git push --tags"
						}
					}

					stage('Publish Nexus') {
						try {
							sh "npm publish --userconfig=/home/jenkins/npmrc --registry ${usilParams.nexusRepoNpmPublish}"
						} catch (err) {
							echo "Erreur rencontrée lors du push : problème Nexus ou Lib (version) déjà existante sur le Nexus"
							currentBuild.result = 'FAILURE'
						}
					}

					stage('Increment version build') {
						withCredentials([usernamePassword(credentialsId: gitCredentialsId, usernameVariable: 'gitPushUsername', passwordVariable: 'gitPushPassword')]) {
							sh 'npm version --no-git-tag-version prerelease --preid=beta'
							sh 'git add package.json'
							sh "git commit -m '[npm version] increment version to ${getPackageJsonVersion()}'"
							sh "git push -u origin ${env.gitBranchName}"
						}
					}
				} else {
					env.artefactVersion = getPackageJsonVersion();
				}
			}
		}
	}
}
