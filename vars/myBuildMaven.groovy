def call(def modeRelease = false, def gitCredentialsId = '', def dockerImageMaven = usilParams.dockerRegistryImageMavenBuild, def mvnOptionnalArgs='') {
	// Pour ne pas relancer, lors du push du pom.xml avec la nouvelle version
	def last_git_comment = sh(returnStdout: true, script: 'git log -1 --pretty=%B')
	if (!last_git_comment.contains("[maven-release-plugin]")) {
		docker.withRegistry(usilParams.dockerRegistryUrl) {
			docker.image(dockerImageMaven).inside('-e MAVEN_CONFIG=/var/maven/.m2 -v /appli/jenkins/settings.xml:/usr/share/maven/ref/settings.xml -v /appli/jenkins/mavenrepo:/var/maven/.m2:rw') {
				stage('MVN compile') {
					sh "mvn clean compile ${mvnOptionnalArgs} -DskipTests=true -Dconsole -Duser.home=/var/maven -U"
				}
				stage('Test Junit') {
					sh "mvn test ${mvnOptionnalArgs} -Duser.home=/var/maven"
				}
				stage('Sonar') {
					withSonarQubeEnv('SONARQUBE_USIL79') {
						sh "mvn sonar:sonar -Dsonar.projectKey=${env.gitProjectName} -Duser.home=/var/maven/.m2 -s /usr/share/maven/ref/settings.xml"
					}
				}
				stage('Quality Gate') {
					def qg = waitForQualityGate()
					if (qg.status != 'OK') {
						error "Pipeline aborted due to quality gate failure: ${qg.status}"
					}
				}
				stage('Publish Nexus') {
					if (modeRelease) {
						withCredentials([usernamePassword(credentialsId: gitCredentialsId, usernameVariable: 'gitPushUsername', passwordVariable: 'gitPushPassword')]) {
							echo "Release maven projet ..."
							sh "git config user.email '${env.trigrammeAppli}@b1envenue.onmicrosoft.com'"
							sh "git config user.name '$env.trigrammeAppli'"
							sh "mvn -B release:prepare release:perform -Dusername=${gitPushUsername} -Dpassword=${gitPushPassword} ${mvnOptionnalArgs} -Duser.home=/var/maven -s /usr/share/maven/ref/settings.xml"
						}
					} else {
						sh "mvn deploy ${mvnOptionnalArgs} -Duser.home=/var/maven -s /usr/share/maven/ref/settings.xml"
					}
					def maven_pom_path = modeRelease ? "${WORKSPACE}/target/checkout/pom.xml" : "${WORKSPACE}/pom.xml";
					def maven_pom_model = readMavenPom file: maven_pom_path;
					env.artefactVersion = maven_pom_model.getVersion();
				}
			}
		}
	}
}
